import React from "react";
import "../styles/picTable.css";
import { useSelector } from "react-redux";
import PicItem from "./PicItem";
import { InitialState } from "./../redux/reducers";

interface PicDisplay {
    hitsReducer: {
        filter: string;
        hits: {
            id: number;
            collections: number;
            downloads: number;
            largeImageURL: string;
            views: number;
            previewURL: string;
        }[];
        page: number;
        pisOnPage: number;
        loading: boolean;
    }
}

const PictureTable = () => {
  const picDisplay = useSelector((state: PicDisplay) => state);
  console.log(picDisplay.hitsReducer.hits);
  return (
    <div className="table">
      {picDisplay.hitsReducer.hits?.map((pic) => {
          console.log(pic);
        return <PicItem picInfo={pic} key={pic.id} />;
      })}
    </div>
  );
};

export default PictureTable;

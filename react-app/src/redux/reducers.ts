import * as types from "./types";

interface Action {
  type: string;
  payload?: any | any[];
}

export interface InitialState {
  filter: string;
  page: number;
  picsOnPage: number;
  hits: {
    largeImageURL: string,
  }[];
  loading: boolean;
}

export const initialState:InitialState = {
  filter: "",
  page: 1,
  picsOnPage: 9,
  hits: [],
  loading: false,
};

export const hitsReducer = (state = initialState, action: Action) => {
  switch (action.type) {
    case types.TRY_GET_DATA:
      return { ...state, loading: true };
    case types.GET_DATA_SUCCESS:
      return {
        ...state,
        hits: action.payload.hits,
        loading: false,
        page: action.payload.page,
        filter: action.payload.filter,
      };
    case types.GET_DATA_FAIL:
      return { ...state, loading: false };
    default:
      // If the reducer doesn't care about action type,
      // return the existing state unchanged
      return state;
  }
};

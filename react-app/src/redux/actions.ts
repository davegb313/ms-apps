import { initialState } from "./reducers";
import * as types from "./types";
import { Request } from "../utils/wrapper";
const initState = { ...initialState };

// Async action creator for GET API Route

interface QueryParams {
  filter: string;
  page: number;
  picsOnPage: number;
  desired: string;
}

const URL = "http://localhost:3001"; // backend local URL

export const getData =
  (queryParams: QueryParams) => async (dispatch: Function) => {
    dispatch({ type: types.TRY_GET_DATA });

    queryParams = {
      filter: initState.filter,
      page: initState.page,
      picsOnPage: initState.picsOnPage,
      desired: queryParams.desired,
    };

    switch (
      queryParams.desired // decide what fetch params send to api
    ) {
      case "":
        break;
      case "next":
        queryParams.page = queryParams.page + 1;
        break;
      case "prev":
        queryParams.page = queryParams.page - 1;
        break;
    }

    const body = await JSON.stringify({
      filter: queryParams.filter,
      page: queryParams.page,
      picsOnPage: queryParams.picsOnPage,
    });

    fetch(URL, {
      method: "Post",
      headers: { "Content-Type": "application/json" },
      body: body,
    })
      .then((res) => {
        return res.json();
      })
      .then((res) => {
        switch (queryParams.desired) {
          case undefined:
            res.desired = undefined;
            break;
          case "next":
            res.desired = "next";
            break;
          case "prev":
            res.desired = "prev";
            break;
        }
        console.log(res);
        dispatch({
          type: "GET_DATA_SUCCESS",
          payload: {
            ...res,
          },
        });
      })
      .catch((err) => {
        dispatch({ type: "GET_DATA_FAIL" });
        console.log("API failed", err);
      });
  };

import React, { useState } from "react";
import "../styles/picItem.css";

const PicItem = (props: {
  picInfo: {
    collections: number;
    downloads: number;
    largeImageURL: string;
    views: number;
    previewURL: string;
  };
}) => {
  const [openModal, setOpenModal] = useState(false);

  const handleOpenModal = () => {
    setOpenModal(true);
  };

  const handleCloseModal = () => {
    setOpenModal(false);
  };

  return (
    <div className="pic-cont">
      {openModal ? (
        <div className="text-cont">
          <button onClick={handleCloseModal}>Close info</button>
          <p>Views: {props.picInfo.views}</p>
          <p>Downloads: {props.picInfo.downloads}</p>
          <p>Collections: {props.picInfo.collections}</p>
          <img
            src={props.picInfo.previewURL}
            alt="pic"
            className="pic-item"
          />
        </div>
      ) : (
        <img
          onClick={handleOpenModal}
          src={props.picInfo.previewURL}
          alt="pic"
          className="pic-item"
        ></img>
      )}
    </div>
  );
};

export default PicItem;

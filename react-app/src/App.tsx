import React from "react";
import { Provider } from "react-redux";
import store from "./redux";
import "./styles/App.css";
import { getData } from "./redux/actions";
import MainPage from "./MainPage";

const App = () => {

  return (
    <Provider {...{ store }}>
        <MainPage/>
    </Provider>
  );
};

export default App;

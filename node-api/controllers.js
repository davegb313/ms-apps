const { handleGetRequest } = require('./api');

const baseUrl = 'https://pixabay.com/api/?key=25540812-faf2b76d586c1787d2dd02736';

const requestData = (req, res, next) => {
  
  let remoteUrl = baseUrl; // base URL with creds
  
  if (req.body) { // Assumed that page and per_Page params are required and exist in every request
    let queryStr = '';
    
    if (req.body.filter?.length) { // if there is a filter => apply it via query params in fetch URL for pixabay
        queryStr = queryStr.concat(`&q=${req.body.filter}`);
    };
    
    
    queryStr = queryStr.concat(`&page=${req.body.page}`, `&per_page=${req.body.picsOnPage}`);
    
    remoteUrl = remoteUrl.concat(queryStr);
  };
  handleGetRequest(req, res, next, remoteUrl);
};

const filterById = (req, res, next) => { 
  let remoteUrl = remoteBaseUrl.concat(`&${req.params.id}`);
  handleGetRequest(req, res, next, remoteUrl);

};

module.exports = {requestData, filterById};

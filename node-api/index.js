const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const { sendDataToClient } = require('./helper');
const { requestData, filterById } = require('./controllers');

const port = 3001;
const app = express();

app.use(cors()); // avoid CORS errors 
app.use(bodyParser.json()); 


app.post('/', requestData, sendDataToClient);

app.get('/:id', filterById, sendDataToClient);

app.listen(port, () => {
  console.log(`Listening on port: ${port}`)
});

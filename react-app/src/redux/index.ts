import { configureStore, applyMiddleware } from "@reduxjs/toolkit";
import thunk from "redux-thunk";

import { hitsReducer } from "./reducers";

const middleware = [thunk];

const store = configureStore(
  {
    // Initialize store
    reducer: {
      hitsReducer: hitsReducer,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(middleware)
  },
);

export default store;
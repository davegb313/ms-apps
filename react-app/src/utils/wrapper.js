import axios from "axios";

export const Request = async (requestParams) => {
  // here it was a fastest way to use switch case construction instead of creating axios instance
  switch (requestParams.method) {
    case "POST":
      try {
        let res = fetch(requestParams.endpoint, {
            method: 'Post',
            headers: { 'Content-Type': 'application/json'},
            body: requestParams.body,
          })
          .then(res => {
            return res.json();
          })
          .then(res => {
            switch (res.desired) {
              case undefined: 
                res.desired = undefined;
                break;
              case 'next':
                res.desired = 'next';
                break;
              case 'prev':
                res.desired = 'prev';
                break;
            };
        })
        if (res) {
          return {
            error: false,
            data: res,
            res,
          };
        } else if (res) {
          console.log(`No data, showing status code instead: ${res} `);
        }
    } catch (err) {
        console.log(err);
      }
  }
};

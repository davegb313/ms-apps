const sendDataToClient = (req, res) => {
    const remoteData = res.locals.remoteData;
    // Send the response data to the client
    res.send(remoteData);
  };
  
  module.exports = { sendDataToClient };
  